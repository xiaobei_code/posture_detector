#pragma once
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <CameraPara.h>

struct CloudBuffer
{
	CloudBuffer(unsigned char * inCloudBuf, int inRows, int inCols) :
		pBuf(inCloudBuf),
		rows(inRows),
		cols(inCols)
	{};
	unsigned char * pBuf;
	int rows;
	int cols;
};
typedef pcl::PointCloud<pcl::PointXYZRGBNormal> PCType;

void buffer2Cloud(PCType::Ptr pCloud, CloudBuffer cloud)
{
	pCloud->height = cloud.rows;
	pCloud->width = cloud.cols;
	pCloud->points.resize(cloud.cols);
	float *pBuf = (float *)cloud.pBuf;
	for (int i = 0; i < pCloud->points.size(); i++)
	{
		pcl::PointXYZRGBNormal & p = pCloud->points[i];
		p.x = *pBuf;
		++pBuf;
		p.y = *pBuf;
		++pBuf;
		p.z = *pBuf;
		++pBuf;
	}
}

