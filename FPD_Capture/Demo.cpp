#include <iostream>
#include <FacePostureDetector.h>
#include <opencv2\opencv.hpp>	

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include "./Demo.h"

using namespace std;

/*
*FacePostureDetector test example
*/

int main(int argc, char * argv[])
{
	printf("Input a folder name\n");
	std::string folderName;
	cin >> folderName;
	if (folderName == "")
	{
		printf("The input folder name is null!\n");
		return 0 ;
	}
	FPD_Demo myDemo(folderName, "landmarks.dat");
	myDemo.RunDemo();
	return 0;
}
