#pragma once

#include "InuStreams.h"
//#include "InuSensorExt.h"
#include "AuxStream.h"
#include <GeneralFrame.h>

#include <iostream>
#include <fstream>
#include <string>
#include <process.h>
#include <iostream>
#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>

#include <Windows.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <conio.h>
#include <direct.h>

#include <thread>

#include <FacePostureDetector.h>
#include <CameraPara.h>
#include <FaceCloudExaminer.h>
#include <FaceReconstructor3.h>
#include <FaceDetection.h>
#include "./build/BufferToPointCloud.h"
#include "./build/FaceImgExaminer.h"

#pragma comment(lib, "Ws2_32.lib")

using namespace InuDev;
/*The image is used to posture detect*/
cv::Mat currentImg;
/*The image is used to display*/
cv::Mat showImg;
/*The point call back frame*/
CImageFrame currentIF;
char* myData;
int Width;
int Height;
/*Used to record current image's face posture*/
int currentFacePosture; //0->front 1->left 2->right
/*If wait before writing ply*/
bool ifPlyHolding;

bool catPly;
bool catImg;

int loop;
/*If write ply*/
bool ifWriteDown;
/*If clone image done*/
bool ifCloneImg;
/*If exist demo*/
bool ifExist;
/*Demo if done*/
bool ifDone;

std::string exeCommand;
std::string folderName;

DrawBadPoint* myDBP;

/*If three face posture we get, wait 3 seconds and exist*/
void Exist()
{
	while (true)
	{
		cv::waitKey(20);
		if (ifExist)
			break;
	}
	std::chrono::milliseconds dura(3000);
	std::this_thread::sleep_for(dura);
	ifDone = true;
}

void ShowCamera()
{
	while (true)
	{
		if (showImg.data != NULL)
		{
			myDBP->draw(showImg);
			cv::imshow("Webcam Image", showImg);
			cv::waitKey(33);
		}
		if (ifDone)
			break;
	}
}

bool WritePLYHeader(const std::string& fileName, int nVertex, bool depthOnly, bool binary)
{
	std::ofstream outPly;
	outPly.open(fileName);
	if (!outPly.good())
	{
		return false;
	}

	outPly << "ply" << std::endl;

	if (binary)
	{
		outPly << "format binary_little_endian 1.0" << std::endl;
	}
	else
	{
		outPly << "format ascii 1.0" << std::endl;
	}

	outPly << "comment CSE generated" << std::endl;
	//outPly<< "element vertex  "<<nVertex + 5<<endl;
	outPly << "element vertex  " << nVertex << std::endl;
	outPly << "property float x" << std::endl;
	outPly << "property float y" << std::endl;
	outPly << "property float z" << std::endl;
	if (!depthOnly)
	{
		outPly << "property uchar red" << std::endl;
		outPly << "property uchar green" << std::endl;
		outPly << "property uchar blue" << std::endl;
		outPly << "property uchar alpha" << std::endl;
	}
	outPly << "end_header" << std::endl;

	outPly.close();
	if (!outPly.good())
	{
		return false;
	}
	return true;
}

void PointCloudCallback(std::shared_ptr<CDepthStream>, const CImageFrame& dFrame, CInuError)
{
	//writeCnt, trigger, writePly,these 3 variables are used as trigger condition simply
	if (!catPly)
		return;
	ifPlyHolding = true;
	ifWriteDown = false;
	
	currentIF.Valid = dFrame.Valid;
	currentIF.FrameIndex = dFrame.FrameIndex;
	Width = dFrame.Width();
	Height = dFrame.Height();
	myData = new char[dFrame.BufferSize()];
	char* newData = (char*)dFrame.GetData();
	for (int i = 0; i < dFrame.BufferSize(); i++)
	{
		myData[i] = newData[i];
	}

	while (ifPlyHolding) {
		cv::waitKey(15);
	}

	if (ifWriteDown)
	{
		printf("Start Write ply....................................................\n!");
		if (currentIF.Valid == false)
		{
			printf("Frame: %ud    is invalid\n", currentIF.FrameIndex);
			return;
		}
		std::string fileName;
		if (currentFacePosture == 0)
			fileName = folderName + "\\front.ply";
		//fileName = "d:/ab/pointcloud_front"+ std::to_string(PlyId) +".ply";
		else if (currentFacePosture == 1)
			fileName = folderName + "\\left.ply";
		//fileName = "d:/ab/pointcloud_left"+ std::to_string(PlyId)+".ply";
		else if (currentFacePosture == 2)
			fileName = folderName + "\\right.ply";
		//fileName = "d:/ab/pointcloud_right"+ std::to_string(PlyId)+".ply";
		else
			fileName = folderName + "\\other.ply";
		if (WritePLYHeader(fileName, Width, true, true) == false)
		{
			printf("Failed to write PLY header, frame: %ud\n", currentIF.FrameIndex);
			return;
		}
		std::ofstream outPly(fileName, std::ios::binary | std::ios::app);
		outPly.write(myData, Height * Width * sizeof(float));
		if (outPly.bad())
		{
			printf("Failed to write PLY contents, frame: %ud\n", currentIF.FrameIndex);
			return;
		}
		else
		{
			printf("write PLY ok...............................!\n");
		}
		outPly.close();
	}

	if (myData != NULL)
	{
		delete[] myData;
	}
	catPly = false;
	loop += 1;
}

void WebCamCallback(std::shared_ptr<CWebCamStream>, const InuDev::CImageFrame&  iFrame, CInuError)
{
	showImg = cv::Mat(iFrame.Height(), iFrame.Width(), CV_8UC4, (uchar*)iFrame.GetData()); //  CV_8UC1  //writeCnt, trigger, writeRGB,these 3 variables are used as trigger condition simply
	if (!catImg)
		return;
	/*Capture current frame*/
	currentImg = showImg.clone();
	//ImgId = iFrame.FrameIndex;
	ifCloneImg = true;
	catImg = false;
	loop += 1;
}

class FPD_Demo
{
public:
	FPD_Demo(std::string userFaceName, char* recognitionModelName)
	{
		std::string fileName = "TestFaceReconstructor3.exe .\\" + userFaceName + 
			"\\left.ply .\\" + userFaceName +"\\front.ply .\\" + userFaceName + "\\right.ply .\\" +
			userFaceName + "\\left.bmp .\\" + userFaceName + "\\front.bmp .\\" + userFaceName + "\\right.bmp .\\" + userFaceName +
			"\\" + userFaceName;
		exeCommand = fileName;
		folderName = ".\\" + userFaceName;
		printf("%s\n", folderName.c_str());
		mkdir(folderName.c_str());
		catPly = false;
		catImg = false;
		loop = 2;
		currentImg.data = NULL;
		fpsp = new FacePostureSetParam();
		myFPD = new FacePostureDetector(*fpsp, recognitionModelName);

		myDBP = new DrawBadPoint();

		bestAngle[0] = 0.0f;
		bestAngle[1] = bestAngle[2] = 0.55f;
		for (int i = 0; i < 3; i++)
		{
			posture[i] = false;
			currentAngle[i] = 10.0f;
		}
		currentFacePosture = -1; //0->front 1->left 2->right
		myData = NULL;
		ifPlyHolding = true;
		ifWriteDown = false;
		ifCloneImg = false;
		ifDone = false;
		ifExist = false;
	}
	~FPD_Demo()
	{
		delete fpsp;
		delete myFPD;
	};

	void RunDemo();

private:
	bool init_sandbox(void);
	bool close_sandbox();
	bool CreateWebcamStream(void);
	bool CreateDepthStream(InuDev::CDepthStream::EOutputType type);

private:
	std::shared_ptr<CInuSensor> sensor;
	std::shared_ptr<CDepthStream> depth;
	std::shared_ptr<CDepthStream> pointcloud;
	std::shared_ptr<CWebCamStream> webcamStream;
	std::shared_ptr<CWebCamStream> webcamStream02;
	std::shared_ptr<CVideoStream> videoStream;
	std::shared_ptr<CGeneralPurposeStream> mGS;
	FacePostureSetParam* fpsp;
	FacePostureDetector* myFPD;
	FPD_LOGS myLogs;
	bool posture[3]; //[0]->front [1]->left [2]->right
	float bestAngle[3]; //front = 0 left = right = 0.55f
	float currentAngle[3];
};

void FPD_Demo::RunDemo()
{
	if (!init_sandbox()) {
		return;
	}
	//create pointcloud stream(without RGB property) and webcam stream(RGB)
	printf("***************Press any key***************\n");
	getch();
	printf("***************Start Demo***************\n");
	CreateDepthStream(InuDev::CDepthStream::ePointCloud);
	CreateWebcamStream();

	std::thread t(ShowCamera);
	std::thread t2(Exist);

	while (!ifDone)
	{
		if (loop == 2)
		{
			ifCloneImg = false;
			catImg = true;
			catPly = true;
			loop = 0;

			while (!ifCloneImg)
			{
				cv::waitKey(15);
			}

			if (currentImg.data != NULL)
			{
				currentFacePosture = -1;
				cv::Mat targetImg;
				cv::cvtColor(currentImg, targetImg, cv::COLOR_BGRA2BGR);
				cv::String fileName;
				myFPD->PostureDetect(&targetImg, myLogs);
				float angle;
				bool ifCloudGood = false;
				if (myLogs.ifHasFace)
				{
					if (myData != NULL)
					{
						std::vector<cv::Point2f> landmarks;
						myFPD->GetLandmarks(landmarks);
						myDBP->SetLandMarks(landmarks);
						//Examine the face point cloud
						CloudBuffer currentCB((unsigned char*)myData, (int)Height, (int)Width);
						buffer2Cloud(myDBP->mCloud, currentCB);
						ifCloudGood = myDBP->FindBadPoints();
					}
					if (myFPD->GetFaceRotationRatio().direction == 1)
					{
						printf("turn left！！！！！！Angle:%f \n", myFPD->GetFaceRotationRatio().faceAngleRatio);
					}
					else
					{
						printf("turn right！！！！！！Angle:%f \n", myFPD->GetFaceRotationRatio().faceAngleRatio);
					}
				}
				if (myLogs.ifSuccess && ifCloudGood)
				{
					printf("----------------------------------Success----------------------------------------------\n");
					angle = myFPD->GetFaceRotationRatio().faceAngleRatio;
					if (angle < 0.07)
					{
						if (abs(angle - bestAngle[0]) < abs(currentAngle[0] - bestAngle[0]))
						{
							currentFacePosture = 0;
							currentAngle[0] = angle;
							fileName = folderName + "\\front.bmp";
							cv::imwrite(fileName.c_str(), targetImg);
							posture[0] = true;
							ifWriteDown = true;
						}
					}
					if (myFPD->GetFaceRotationRatio().direction == 1)
					{
						if (angle > 0.45f && angle < 0.6f)
						{
							if (abs(angle - bestAngle[1]) < abs(currentAngle[1] - bestAngle[1]))
							{
								currentFacePosture = 1;
								currentAngle[1] = angle;
								fileName = folderName + "\\left.bmp";
								cv::imwrite(fileName.c_str(), targetImg);
								posture[1] = true;
								ifWriteDown = true;
							}
						}
					}
					else
					{
						if (angle > 0.45f && angle < 0.6f)
						{
							if (abs(angle - bestAngle[2]) < abs(currentAngle[2] - bestAngle[2]))
							{
								currentFacePosture = 2;
								currentAngle[2] = angle;
								fileName = folderName + "\\right.bmp";
								cv::imwrite(fileName.c_str(), targetImg);
								posture[2] = true;
								ifWriteDown = true;
							}
						}
					}
				}
				else
				{
					myLogs.Show();
				}
				ifPlyHolding = false;
			}
		}
		bool ifGetAllFace = true;
		for (int i = 0; i < 3; i++)
		{
			if (!posture[i])
				ifGetAllFace = false;
		}
		if (ifGetAllFace)
		{
			ifExist = true;
			printf("Get All Face! Wait......and keep rotating your face!\n");
		}
	}
	t.join();
	t2.join();
	while (loop != 2)
	{
		printf("Wait write img and ply......\n");
		cv::waitKey(20);
	}
	close_sandbox();
	system(exeCommand.c_str());
}


bool FPD_Demo::close_sandbox(void)
{
	CInuError err(eOK);
	err = sensor->Terminate();
	if (err != eOK)
	{
		printf("error: %s \n", ((std::string)err).c_str());
		return false;

	}
	return true;
}

bool FPD_Demo::CreateDepthStream(InuDev::CDepthStream::EOutputType type)
{
	CInuError err(eOK);

	pointcloud = sensor->CreateDepthStream();

	err = pointcloud->Init(type);
	if (err != eOK)
	{
		printf("Pointcloud stream failed to Init\n");
		return false;
	}

	err = pointcloud->Start();
	if (err != eOK)
	{
		printf("Pointcloud stream failed to Start\n");
		return false;
	}

	pointcloud->Register(PointCloudCallback);
	printf("Pointcloud stream Started\n");
	return true;
}

bool FPD_Demo::CreateWebcamStream(void)
{
	CInuError err(eOK);
	webcamStream = sensor->CreateWebCamStream();
	err = webcamStream->Init();
	if (err != eOK)
	{
		printf("webcam stream failed to Init\n");
		return false;
	}
	err = webcamStream->Start();
	if (err != eOK)
	{
		printf("Webcam stream failed to Start\n");
		return false;
	}
	webcamStream->Register(WebCamCallback);
	printf("WebCam stream  Started\n");
	return true;
}

bool FPD_Demo::init_sandbox(void)
{
	sensor = CInuSensor::Create();
	sensor = CInuSensor::Create();
	CSensorParams prm;
	prm.SensorRes = eFull;// eFull;
						  //prm.SensorRes = eBinning; // -VGA
	prm.FPS = 30;
	CInuError err(eOK);
	err = sensor->Init(prm);
	if (err != eOK)
	{
		printf("InuSensor failed to Init\n");
		return false;
	}
	printf("InuSensor Init\n");
	err = sensor->Start();
	if (err != eOK)
	{
		printf("InuSensor failed to Start\n");
		return false;
	}
	printf("Sensor Started\n");
	InuDev::CSensorControlParams params;
	sensor->GetSensorControlParams(params);
	params.AutoControl = true; // true;
							   //params.ExposureTimeRight = 5000;
							   //params.ExposureTimeLeft = 5000;
	sensor->SetSensorControlParams(params);
	printf("SetSensorControlParams Done\n");
	return(true);
}