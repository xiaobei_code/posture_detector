#pragma once
#include <FaceCloudExaminer.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <opencv2/opencv.hpp>    
#include <FaceDetection.h>

class DrawBadPoint
{
public:
	DrawBadPoint();
	~DrawBadPoint() {};
	bool FindBadPoints();
	void SetLandMarks(LandMarks & landmarks);

public:
	PCType::Ptr mCloud;
	void draw(cv::Mat & img);

private:
	void setConfig();
	void storeBadPoints(std::vector<cv::Point> & badPointsPixels);
	bool ifStorePoints;

private:
	CameraPara cPara;
	LandMarks myLandMarks;
	std::vector<cv::Point> mBadPointsPixels;
}; 

inline DrawBadPoint::DrawBadPoint()
{
	ifStorePoints = false;
	mCloud = PCType::Ptr(new PCType);
	setConfig();
	myLandMarks.reserve(68);
}

bool DrawBadPoint::FindBadPoints() {
	FaceCloudExaminer fcm(cPara);
	bool returnVal = fcm.examineCloud(mCloud, myLandMarks);
	storeBadPoints(fcm.getBadPointPixels());
	return returnVal;
}

void DrawBadPoint::SetLandMarks(LandMarks & landmarks)
{
	for (int i = 0; i < 68; i++)
	{
		myLandMarks[i] = landmarks[i];
	}
}

void DrawBadPoint::storeBadPoints(std::vector<cv::Point> & badPointsPixels)
{
	ifStorePoints = true;
	mBadPointsPixels.clear();
	for (int i = 0; i < badPointsPixels.size(); i++)
	{
		mBadPointsPixels.push_back(badPointsPixels[i]);
	}
	ifStorePoints = false;
}

void DrawBadPoint::draw(cv::Mat & img)
{
	if (!ifStorePoints)
	{
		bool drawBlack = true;
		for (int i = 0; i < mBadPointsPixels.size(); i++)
		{
			int x = mBadPointsPixels[i].x;
			int y = mBadPointsPixels[i].y;
			x = x > 0 ? x : 0;
			y = y > 0 ? y : 0;
			x = x < img.cols ? x : img.cols - 1;
			y = y < img.rows ? y : img.rows - 1;
			cv::Point p(x, y);
			if (drawBlack)
			{
				cv::circle(img, p, 3, cv::Scalar(0, 0, 0), -1);
				drawBlack = false;
			}
			else
			{
				cv::circle(img, p, 3, cv::Scalar(0, 0, 255), -1);
				drawBlack = true;
			}
		}
	}
}

void DrawBadPoint::setConfig()
{
	//float RotationRealWebcam[3] = { -0.002185f, -0.007537f, 0.002169f };
	//float TranslationRealWebcam[3] = { 6.803672f, -0.021485f, 0.557242f };
	//float LensDistortionsRealWebcam[5] = { 0.223061f,-1.040915f,-0.002529f, 0.000232f ,0.995493f };
	//float FocalLengthRealWebcam[2] = { 1472.449597f,1472.449597f };
	//float OpticalCenterRealWebcam[2] = { 621.911586f,448.674758f };

	float RotationRealWebcam[3] = { -0.002922f, 0.005027f, -0.002997f };
	float TranslationRealWebcam[3] = { 7.397930f, -0.795893f, -0.048076f };
	float LensDistortionsRealWebcam[5] = { 0.240535f, -1.081375f, -0.002271f, 0.000013f, 0.868565f };
	float FocalLengthRealWebcam[2] = { 1481.778408f, 1481.778408f };
	float OpticalCenterRealWebcam[2] = { 630.737994f, 452.188215f };

	cPara.FocalLengthRealWebcam[0] = FocalLengthRealWebcam[0];
	cPara.FocalLengthRealWebcam[1] = FocalLengthRealWebcam[1];

	cPara.LensDistortionsRealWebcam[0] = LensDistortionsRealWebcam[0];
	cPara.LensDistortionsRealWebcam[1] = LensDistortionsRealWebcam[1];
	cPara.LensDistortionsRealWebcam[2] = LensDistortionsRealWebcam[2];
	cPara.LensDistortionsRealWebcam[3] = LensDistortionsRealWebcam[3];
	cPara.LensDistortionsRealWebcam[4] = LensDistortionsRealWebcam[4];

	cPara.RotationRealWebcam[0] = RotationRealWebcam[0];
	cPara.RotationRealWebcam[1] = RotationRealWebcam[1];
	cPara.RotationRealWebcam[2] = RotationRealWebcam[2];

	cPara.TranslationRealWebcam[0] = TranslationRealWebcam[0];
	cPara.TranslationRealWebcam[1] = TranslationRealWebcam[1];
	cPara.TranslationRealWebcam[2] = TranslationRealWebcam[2];

	cPara.OpticalCenterRealWebcam[0] = OpticalCenterRealWebcam[0];
	cPara.OpticalCenterRealWebcam[1] = OpticalCenterRealWebcam[1];
}