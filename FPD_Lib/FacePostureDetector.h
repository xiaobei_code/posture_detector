#ifndef _FACE_POSTURE_DETECTOR_H_
#define _FACE_POSTURE_DETECTOR_H_
#ifdef FACE_POSTURE_DETECTOR_DLL_EXPORTS
#define FACE_POSTURE_DETECTOR_DLL_API __declspec(dllexport)
#else
#define FACE_POSTURE_DETECTOR_DLL_API __declspec(dllimport)
#endif
#include <string>
#include <opencv2/opencv.hpp>

#define MAX_LOGS_NUM 8

struct FacePostureSetParam
{
	/*
	*In order to speed up the face recognition
	*we need to narrow the original image
	*(int)mStandardImgLen is side length of the shrunken image
	*/
	int mStandardImgLen = 400;
	/*
	*In order to ensure that the (main)recognized face is in the center of the image
	*we should limit the face's center in the image's center (rect)range
	*(float)mFaceCenterRange is the ratio of the (rect)range's side length to the original image's shorest side length
	*mFaceCenterRange = (rect)range's side length / the original image's shortest side length.
	*/
	float mFaceCenterRange = 0.2f;
	/*
	*To ensure that the face is not too close to the camera(make sure the face is not too large in image)
	*(float)mMaxFaceLenRatio is the ratio of the face's rect side length to the orginal image's shorest side length
	*mMaxFaceLenRatio = (rect)face's side length / the original image's shortest side length.
	*/
	float mMaxFaceLenRatio = 0.8f;
	/*
	*To ensure that the face is not too close to the camera(make sure the face is not too large in image)
	*(float)mMinFaceLenRatio is the ratio of the face's rect side length to the orginal image's shorest side length
	*mMinFaceLenRatio = (rect)face's side length / the original image's shortest side length.
	*/
	float mMinFaceLenRatio = 0.3f;

	//We set the front face's angle(ratio) is 0, and think the face turn left(right) to 90 degrees is 1
	//Than we think the face's angle(ratio) is between 0 and 1 (angle��[0,1])
	/*
	*In order to identify the front face, we set a threshold to limit the face ratation angle(ratio) in x-axis
	*If It's angle(ratio) less than the threshold
	*Than we think the current face is the front face
	*The ratation angle(ratio) is in x-axis
	*/
	float mMaxFrontFaceAngleRatio = 0.1f;
	/*
	*In order to identify the side face, we set a threshold to limit the face ratation angle(ratio) in x-axis
	*If It's angle(ratio) between the mMaxSideFaceAngleRatio and the mMinSideFaceAngleRatio
	*Than we think the current face is the side face
	*The ratation angle(ratio) is in x-axis
	*/
	float mMaxSideFaceAngleRatio = 0.65f;
	float mMinSideFaceAngleRatio = 0.40f;

	/*
	*In order to ensure that the face plane is parallel to the camera plane
	*We set a threshold to limit the face ratation angle(ratio) in y-axis
	*If it's angle(ratio) less than threshold
	*Than we think the current face is facing the camera
	*The ratation angle(ratio) is in y-axis
	*/
	float mMaxFaceAngleYRatio = 1.2f;

	/*
	*To avoid the face closing eyes
	*We set a threshold to judge if face's eye(s) is(are) closed
	*If it's value less than threshold
	*Than we think the eye(s) is(are) closed
	*/
	float mEyeClosedThreshold = 0.15f;
};

struct FaceRotation
{
	int direction; // 0->doesn't turn	1->turn left(right face)	2->turn right(left face)
	float faceAngleRatio; // Rotation ration, it's value��[0,1]
};

enum FPD_LOG
{
	NoFaceInfo = 1, //Current image doesn't have face information
	FaceCenterOut = 2, //Current face's center is out image center rect range
	FaceTooBig = 3, //Current face's area is too big (Current face is too close the camera)
	FaceTooSmall = 4, //Current face's area is too small (Current face is too far from the camera)
	EyeClosed = 5, //Current face's eye(s) is(are) closed
	WearGlasses = 6, //Current face is wearing the glasses
	FaceNoParallelCamera = 7, //Current face's isn't parallel the camera plane
	FacePostureIsWrong = 8 //Current face's angle ratio in x-axis is wrong
};

/*
*A vector to store FPD_LOG
*By the FPD_LOGS, we can know what happened during the period of posture detection!
*/
struct FPD_LOGS
{
	/*
	*Vale name: ifSuccess
	*Return True : The posture of the face in the current image corresponds to the data we need!
	*Return	False : The posture of the face in the current image doesn't correspond to the data we need!
	*/
	bool ifSuccess = true; 
	/*
	*Vale name: ifHasFace
	*Return True : Current image has face information!
	*Return	False : There isn't any face information in current image!
	*/
	bool ifHasFace = false;
	/*Logs number*/
	int num = 0;
	/*The vector of logs*/
	FPD_LOG mLogs[MAX_LOGS_NUM];
	/*When use FPD_LOGS, we had better clear it*/
	void Clear() 
	{
		ifSuccess = true;
		ifHasFace = false;
		num = 0;
	}
	/*Add a log into logs*/
	void AddLog(FPD_LOG & myLog)
	{
		if ((num + 1) > MAX_LOGS_NUM)
		{
			printf("Current logs is full and can't add any log!");
			return;
		}
		mLogs[num] = myLog;
		ifSuccess = false;
		num++;
	}
	/*Show logs' detail*/
	void Show()
	{
		for (int i = 0; i < num; i++)
		{
			switch (mLogs[i])
			{
			case FPD_LOG::EyeClosed:
				printf("Current face's eye(s) is(are) closed!\n");
				break;
			case FPD_LOG::FaceCenterOut:
				printf("Current face's center is out image center rect range!\n");
				break;
			case FPD_LOG::FaceNoParallelCamera:
				printf("Current face's isn't parallel the camera plane!\n");
				break;
			case FPD_LOG::FacePostureIsWrong:
				printf("Current face's angle ratio in x-axis is wrong!\n");
				break;
			case FPD_LOG::FaceTooBig:
				printf("Current face's area is too big (Current face is too close the camera)!\n");
				break;
			case FPD_LOG::FaceTooSmall:
				printf("Current face's area is too small (Current face is too far from the camera)!\n");
				break;
			case FPD_LOG::NoFaceInfo:
				printf("Current image doesn't have face information!\n");
				break;
			case FPD_LOG::WearGlasses:
				printf("Current face is wearing the glasses!\n");
				break;
			default:
				break;
			}
		}
	}
};

class FacePostureDetectorImp;

class FACE_POSTURE_DETECTOR_DLL_API FacePostureDetector
{
public:
	/*
	*Construction
	*Set the FacePostureDetector
	\param fpsp, the correlated setting param about FacePostureDetector
	\param RecognitionModelName, it's default value = "landmarks.dat"
	*/
	FacePostureDetector(FacePostureSetParam fpsp, std::string RecognitionModelName = "landmarks.dat");
	/*
	*Deconstruction
	*/
	~FacePostureDetector();

	//FacePostureDetector Interface

	/*
	*Detect if current image have legal face (right posture)
	\param img(24 depth), the image needed to detect
	\param logs, record running information
	*/
	bool PostureDetect(const cv::Mat * myImg, FPD_LOGS & myLogs);	

	/*
	*Detect if current image have legal face (right posture)
	\param pBuf(32 depth), the image's data 
	\param Height, the image's rows
	\param Width, the image's cols
	\param logs, record running information
	*/
	bool PostureDetect(unsigned char * pBuf, int Height, int Width, FPD_LOGS & myLogs);

	/*
	*Get the current face rotation's ratio
	*/
	FaceRotation & GetFaceRotationRatio();

	/*
	*Get the (68)landmarks of face's features
	*/
	void GetLandmarks(std::vector<cv::Point2f> & landmarks);

private:
	FacePostureDetectorImp * mFPDI;
};

#endif //_FACE_POSTURE_DETECTOR_H_






