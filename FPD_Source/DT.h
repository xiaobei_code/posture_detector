#pragma once

#include <iostream>
#include <dlib/image_io.h>
#include <ctime>
using namespace dlib;

namespace cg
{
	class DT
	{
		//protected:
	public:
		DT() {
			detector = get_frontal_face_detector();
		}
		array2d<rgb_pixel> m_image;
		frontal_face_detector detector;
		shape_predictor m_sp;
	};
}