#define FACE_DETECTION_EXPORTS
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>

#include "./DT.h"
#include "./FaceDetection.h"

cg::FaceDetection::FaceDetection()
{
	m_dt = new DT();
}

cg::FaceDetection::~FaceDetection()
{
	delete m_dt;
}

int cg::FaceDetection::load_image(const std::string & input)
{
	try
	{
		dlib::load_image(m_dt->m_image, input);
	}
	catch (std::exception& e)
	{
		std::cout << "\nLoad image exception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	return 0;
}

int cg::FaceDetection::load_image(const unsigned char * data, const size_t rows, const size_t cols, const size_t channels)
{
	try
	{
		m_dt->m_image.set_size(rows, cols);
		for (size_t r = 0; r < rows; ++r)
			for (size_t c = 0; c < cols; ++c)
			{
				size_t pos = cols * channels * r + channels * c;
				m_dt->m_image[r][c].blue = data[pos];
				m_dt->m_image[r][c].green = data[pos + 1];
				m_dt->m_image[r][c].red = data[pos + 2];
			}
	}
	catch (std::exception& e)
	{
		std::cout << "\nLoad image exception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	return 0;
}

int cg::FaceDetection::exact_face(std::vector<dlib::rectangle>& rects)
{
	try
	{
		rects = m_dt->detector(m_dt->m_image);
	}
	catch (std::exception& e)
	{
		std::cout << "\nExact face exception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	return 0;
}

int cg::FaceDetection::pre_detect_landmarks(const std::string & shape_landmarks)
{
	try
	{
		if (shape_landmarks.empty())
			deserialize("landmarks.dat") >> m_dt->m_sp;
		else
			deserialize(shape_landmarks) >> m_dt->m_sp;
	}
	catch (std::exception& e)
	{
		std::cout << "\nPrepare detect landmarks exception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	return 0;
}

int cg::FaceDetection::exact_landmarks(const dlib::rectangle & rect, std::vector<cv::Point2f> & landmarks)
{
	try {
		std::vector<full_object_detection> shapes;
		dlib::rectangle face_rect(rect.left(), rect.top(), rect.right(), rect.bottom());
		full_object_detection shape = m_dt->m_sp(m_dt->m_image, face_rect);

		for (size_t i = 0; i < shape.num_parts(); ++i)
		{
			landmarks[i].x = shape.part(i).x();
			landmarks[i].y = shape.part(i).y();
		}
	}
	catch (std::exception& e)
	{
		std::cout << "\nExact landmarks exception thrown!" << std::endl;
		std::cout << e.what() << std::endl;
		return -1;
	}
	return 0;
}