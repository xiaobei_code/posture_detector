#ifndef _FACE_DETECTION_H_
#define _FACE_DETECTION_H_

#ifdef FACE_DETECTION_EXPORTS
#define FACE_DETECTION_API __declspec(dllexport)
#else
#define FACE_DETECTION_API __declspec(dllimport)
#endif

#ifdef FACE_DETECTION_EXPORTS
#define FACE_DETECTION_API __declspec(dllexport)
#else
#define FACE_DETECTION_API __declspec(dllimport)
#endif // FACE_DETECTION_EXPORTS


#include <cstdlib>
#include <string>
#include <vector>
#include <dlib/opencv.h>
#include <opencv2/opencv.hpp>

namespace cg
{
	class DT;

	class FACE_DETECTION_API FaceDetection
	{
	public:
		FaceDetection();
		~FaceDetection();

		/* \brief input the texture image
		* \input texture
		* \return success:0, otherwise:!0
		*/
		int load_image(const std::string & input);

		/* \brief input the texture image
		* \input unsigned char pointer, which is usually the data from opencv Mat(CV_8UC3)
		* \input the number of rows of the image
		* \input the number of columns of the image
		* \input the number of channels of the image
		* \return success:0, otherwise:!0
		*/
		int load_image(const unsigned char * data, const size_t rows, const size_t cols, const size_t channels);

		/* \brief exact the face rectangles
		* \ourput a group of rectangles which contain all faces detected
		* \return success:0, otherwise:!0
		*/
		int exact_face(std::vector<dlib::rectangle> & rects);

		/* \brief prepare work for detecting landmarks, this step will consume much time
		* \input default is OK
		* \return success:0, otherwise:!0
		*/
		int pre_detect_landmarks(const std::string & shape_landmarks = "");

		/* \brief exact landmarks for the input face rectangle
		* \input a rectangle which contains a face
		* \output the 68 landmarks on the face
		* \return success:0, otherwise:!0
		*/
		int exact_landmarks(const dlib::rectangle & rect, std::vector<cv::Point2f> & landmarks);
	private:
		DT * m_dt;
	};
}

#endif //_FACE_DETECTION_H_