#define FACE_POSTURE_DETECTOR_DLL_EXPORTS
#include "./FacePostureDetector.h"
#include <FaceDetection.h>
#include <dlib/opencv.h>    
#include <opencv2/opencv.hpp>    
#include <dlib/image_processing/frontal_face_detector.h>   
#include <dlib/image_processing/render_face_detections.h>    
#include <dlib/image_processing.h>    
#include <dlib/gui_widgets.h>

class FacePostureDetectorImp
{
public:
	FacePostureDetectorImp
	(FacePostureSetParam fpsp, std::string RecognitionModelName) : mFacePostureSetParam(fpsp)
	{
		/*Judge if the RecognitionModel exist*/
		FILE* fp = fopen(RecognitionModelName.c_str(), "r");
		if (fp == NULL)
		{
			printf("The RecognitionModel (%s) doesn't exist!", RecognitionModelName.c_str());
			return;
		}
		fd.pre_detect_landmarks(RecognitionModelName);
		myFaceRotationRatio.direction = 0;
		myFaceRotationRatio.faceAngleRatio = 0;
		mLandMarks.reserve(68);
	};
	~FacePostureDetectorImp() {};
	/*Detect if current image have legal face*/
	bool PostureDetect(const cv::Mat * img, FPD_LOGS & myLogs);
	/*Get the current face rotation*/
	FaceRotation & GetFaceRotationRatio();
	/*Get the face landmarks*/
	void GetLandMarks(std::vector<cv::Point2f> & landMarks);

private:
	/*Judge if current image has face's information*/
	bool IfHasFace(const cv::Mat * img, std::vector<dlib::rectangle> & rects);
	/*Get the rotation angle ratio in x-asix*/
	void GetFaceRotationX();
	/*Judge if the current face has legal position and legal size*/
	void IfPositionLegal(const cv::Mat * img, FPD_LOGS & myLogs);
	/*Judge if current face is closing eye*/
	bool IfClosingEye();
	/*Get the rotation of face*/
	void IfRotationLegal(FPD_LOGS & myLogs);
	/*Store the face's rect*/
	void StoreFaceRect(dlib::rectangle & currentFR);
	/*Clear the face's rect*/
	void ClearFaceRect();
	/*Clear the face's landmarks*/
	void ClearLandMarks();

private:
	float distanceTwoPoints(cv::Point2i point1, cv::Point2i point2);
	float PointToLine(cv::Point2i tarPoint, cv::Point2i point1, cv::Point2i point2);

private:
	/* The face detection */
	cg::FaceDetection fd;
	/* The param of FacePosture */
	FacePostureSetParam mFacePostureSetParam;
	/* Current face roatation */
	FaceRotation myFaceRotationRatio;
	/* Current face's rect */
	dlib::rectangle mFaceRect;
	/* Current face's landmarks */
	std::vector<cv::Point2f> mLandMarks;
};

inline void FacePostureDetectorImp::GetFaceRotationX()
{
	float leftDis2 = distanceTwoPoints(mLandMarks[39], mLandMarks[27]);
	float rightDis2 = distanceTwoPoints(mLandMarks[42], mLandMarks[27]);
	myFaceRotationRatio.direction = (leftDis2 > rightDis2) ? 1 : 2;
	myFaceRotationRatio.faceAngleRatio = (leftDis2 > rightDis2) ? (1 - rightDis2 / leftDis2) : (1 - leftDis2 / rightDis2);
}

inline void FacePostureDetectorImp::StoreFaceRect(dlib::rectangle & currentFR)
{
	mFaceRect.set_bottom(currentFR.bottom());
	mFaceRect.set_left(currentFR.left());
	mFaceRect.set_right(currentFR.right());
	mFaceRect.set_top(currentFR.top());
}

inline void FacePostureDetectorImp::ClearLandMarks()
{
	for (unsigned int i = 0; i < 68; i++)
	{
		mLandMarks[i].x = 0;
		mLandMarks[i].y = 0;
	}
}

inline void FacePostureDetectorImp::ClearFaceRect()
{
	mFaceRect.set_bottom(0);
	mFaceRect.set_top(0);
	mFaceRect.set_left(0);
	mFaceRect.set_right(0);
}

inline void FacePostureDetectorImp::GetLandMarks(std::vector<cv::Point2f> & landMarks)
{
	for (unsigned int i = 0; i < 68; i++)
	{
		landMarks[i].x = mLandMarks[i].x;
		landMarks[i].y = mLandMarks[i].y;
	}
}

inline FaceRotation &  FacePostureDetectorImp::GetFaceRotationRatio()
{
	return myFaceRotationRatio;
}

inline bool FacePostureDetectorImp::PostureDetect(const cv::Mat * img, FPD_LOGS & myLogs)
{
	FPD_LOG myLog;
	/*Clear*/
	ClearFaceRect();
	ClearLandMarks();
	myLogs.Clear();

	int shortestSide = (img->cols < img->rows) ? img->cols : img->rows;
	int ratio = (shortestSide > mFacePostureSetParam.mStandardImgLen) ? shortestSide / mFacePostureSetParam.mStandardImgLen : 1;
	
	/*Scaling the source image*/
	cv::Mat scalingImg;
	cv::resize(*img, scalingImg, cv::Size(img->cols / ratio, img->rows / ratio));
	/*Judge if current frame has faces information*/
	std::vector<dlib::rectangle> facesRects;
	if (IfHasFace(&scalingImg, facesRects) == false)
	{
		myLog = FPD_LOG::NoFaceInfo;
		myLogs.AddLog(myLog);
		myLogs.ifHasFace = false;
		return false;
	}
	else
	{
		myLogs.ifHasFace = true;
	}
	/*Find the biggest face*/
	dlib::rectangle * biggestFace = NULL;
	/*Refine the facesRect and find the biggest one of face's area*/
	for (unsigned long i = 0; i < facesRects.size(); ++i)
	{
		facesRects[i].set_bottom(facesRects[i].bottom() * ratio);
		facesRects[i].set_top(facesRects[i].top() * ratio);
		facesRects[i].set_left(facesRects[i].left() * ratio);
		facesRects[i].set_right(facesRects[i].right() * ratio);
		if (biggestFace == NULL)
			biggestFace = &facesRects[i];
		else
		{
			if (facesRects[i].area() > biggestFace->area())
				biggestFace = &facesRects[i];
		}
	}
	/*Store the current face's rect*/
	StoreFaceRect(*biggestFace);
	/*Find the current face's 68 landmarks*/
	fd.load_image(img->data, img->rows, img->cols, img->channels());
	fd.exact_landmarks(mFaceRect, mLandMarks);
	/*Get the rotation angle ratio in x-asix*/
	GetFaceRotationX();
	/*Judge if the biggest face has legal position and legal size*/
	IfPositionLegal(img, myLogs);
	/*Judge if the face's roatation is legal*/
	IfRotationLegal(myLogs);
	/*Judge if current face is closing eye*/
	if (IfClosingEye())
	{
		FPD_LOG myLog = FPD_LOG::EyeClosed;
		myLogs.AddLog(myLog);
	}
	if (myLogs.ifSuccess)
		return true;
	else
		return false;
}

/*Judge if current image has face's information*/
inline bool FacePostureDetectorImp::IfHasFace(const cv::Mat * img, std::vector<dlib::rectangle> & facesRects)
{
	fd.load_image(img->data, img->rows, img->cols, img->channels());
	fd.exact_face(facesRects);
	if (facesRects.size() > 0)
		return true;
	else
		return false;
}

/*Judge if the current face has legal position and legal size*/
inline void FacePostureDetectorImp::IfPositionLegal(const cv::Mat * img, FPD_LOGS & myLogs)
{
	cv::Point2i imgCenter(img->cols / 2, img->rows / 2);
	int faceCenterSide = (mFaceRect.right() - mFaceRect.left()) / 2;
	cv::Point2i faceCenter(faceCenterSide + mFaceRect.left(), faceCenterSide + mFaceRect.top());
	int shortestSide = (img->cols < img->rows) ? img->cols : img->rows;
	int faceCenterRage = mFacePostureSetParam.mFaceCenterRange * shortestSide;
	/*Judge if the range of faceRect's center is legal*/
	if (faceCenter.x >(imgCenter.x + faceCenterRage)
		|| faceCenter.x < (imgCenter.x - faceCenterRage)
		|| faceCenter.y >(imgCenter.y + faceCenterRage)
		|| faceCenter.y < (imgCenter.y - faceCenterRage))
	{
		FPD_LOG myLog = FPD_LOG::FaceCenterOut;
		myLogs.AddLog(myLog);
	}
	/*Judge if the length of faceRect's side is more than threshold*/
	if (mFaceRect.width() >(shortestSide * mFacePostureSetParam.mMaxFaceLenRatio))
	{
		FPD_LOG myLog = FPD_LOG::FaceTooBig;
		myLogs.AddLog(myLog);
	}
	/*Judge if the length of faceRect's side is less than threshold*/
	if (mFaceRect.width() < (shortestSide * mFacePostureSetParam.mMinFaceLenRatio))
	{
		FPD_LOG myLog = FPD_LOG::FaceTooSmall;
		myLogs.AddLog(myLog);
	}
}

/*Judge if current face is closing eye*/
inline bool FacePostureDetectorImp::IfClosingEye()
{
	/*Left eye's width*/
	float leftEyeWidth = distanceTwoPoints(mLandMarks[36], mLandMarks[39]);
	float leftEyeHeight = distanceTwoPoints(mLandMarks[38], mLandMarks[40]);
	float rightEyeWidth = distanceTwoPoints(mLandMarks[42], mLandMarks[45]);
	float rightEyeHeight = distanceTwoPoints(mLandMarks[43], mLandMarks[47]);
	return (leftEyeHeight < leftEyeWidth * mFacePostureSetParam.mEyeClosedThreshold || rightEyeHeight < rightEyeWidth * mFacePostureSetParam.mEyeClosedThreshold);
}

/*Get the rotation of face*/
inline void FacePostureDetectorImp::IfRotationLegal(FPD_LOGS & myLogs)
{
	float standardDis = distanceTwoPoints(mLandMarks[27], mLandMarks[28]);
	float leftDis = PointToLine(mLandMarks[36], mLandMarks[0], mLandMarks[16]);
	float rightDis = PointToLine(mLandMarks[45], mLandMarks[0], mLandMarks[16]);
	float Dis = (leftDis > rightDis) ? leftDis : rightDis;
	if (Dis > standardDis * mFacePostureSetParam.mMaxFaceAngleYRatio)
	{
		FPD_LOG myLog = FPD_LOG::FaceNoParallelCamera;
		myLogs.AddLog(myLog);
	}

	/*Judge if current face is front face*/
	if (myFaceRotationRatio.faceAngleRatio > mFacePostureSetParam.mMaxFrontFaceAngleRatio)
	{
		/*If current face isn't front face*/
		/*Judge if current face is side face*/
		if (myFaceRotationRatio.faceAngleRatio < mFacePostureSetParam.mMinSideFaceAngleRatio
			|| myFaceRotationRatio.faceAngleRatio > mFacePostureSetParam.mMaxSideFaceAngleRatio)
		{
			FPD_LOG myLog = FPD_LOG::FacePostureIsWrong;
			myLogs.AddLog(myLog);
		}
	}
}

inline float FacePostureDetectorImp::distanceTwoPoints(cv::Point2i point1, cv::Point2i point2)
{
	int x1 = point1.x;
	int y1 = point1.y;
	int x2 = point2.x;
	int y2 = point2.y;
	return (sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
}

inline float FacePostureDetectorImp::PointToLine(cv::Point2i tarPoint, cv::Point2i point1, cv::Point2i point2)
{
	int tarX = tarPoint.x;
	int tarY = tarPoint.y;
	int x1 = point1.x;
	int y1 = point1.y;
	int x2 = point2.x;
	int y2 = point2.y;
	float A = y2 - y1;
	float B = x1 - x2;
	float C = x2 * y1 - x1 * y2;
	float distance = abs(A * tarX + B * tarY + C) / sqrt(A * A + B * B);
	return distance;
}

///////////////////////////////////////////////

/*
*Construction
*Set the FacePostureDetector
\param fpsp, the correlated setting param about FacePostureDetector
\param RecognitionModelName, it's default value = "landmarks.dat"
*/
FacePostureDetector::FacePostureDetector(FacePostureSetParam fpsp, std::string RecognitionModelName)
{
	mFPDI = new FacePostureDetectorImp(fpsp, RecognitionModelName);
}

/*
*Deconstruction
*/
FacePostureDetector::~FacePostureDetector()
{
	delete mFPDI;
}

/*
*Detect if current image have legal face (right posture)
\param img, the image needed to detect
\param logs, record running information
*/
inline bool FacePostureDetector::PostureDetect(const cv::Mat * myImg, FPD_LOGS & myLogs)
{
	return mFPDI->PostureDetect(myImg, myLogs);
}

/*
*Detect if current image have legal face (right posture)
\param img, the image needed to detect
\param log, record running information
*/
bool FacePostureDetector::PostureDetect(unsigned char * pBuf, int Height, int Width, FPD_LOGS & myLogs)
{
	cv::Mat img(Height, Width, CV_8UC4, pBuf);
	cv::Mat targetImg;
	cv::cvtColor(img, targetImg, cv::COLOR_BGRA2BGR);
	return mFPDI->PostureDetect(&targetImg, myLogs);
}

/*
*Get the current face rotation's ratio
*/
FaceRotation & FacePostureDetector::GetFaceRotationRatio()
{
	return mFPDI->GetFaceRotationRatio();
}

/*
*Get the (68)landmarks of face's features
*/
inline void FacePostureDetector::GetLandmarks(std::vector<cv::Point2f> & landmarks)
{
	if (landmarks.capacity() < 68)
		landmarks.resize(68);
	mFPDI->GetLandMarks(landmarks);
}














